const mongoose = require('mongoose');
const { Schema } = mongoose;

const loginLogSchema = new Schema({
    email: { type: String, require: true, trim: true },
    ip: { type: String, require: true, trim: true },
    userAgent: { type: String, require: true, trim: true },
    success: { type: Boolean, require: true, trim: true },
    createdAt: { type: String, require: true, trim: true },
});

module.exports = mongoose.model('LoginLog', loginLogSchema);
