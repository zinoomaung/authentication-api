const mongoose = require('mongoose');
const { Schema } = mongoose;

const favouriteCardSchema = new Schema({
    cardId: { type: String, require: true, trim: true },
    user: { type: Schema.Types.ObjectId, require: true, ref: 'User' },
    isFavourite: { type: Boolean, require: true, default: true },
});

module.exports = mongoose.model('FavouriteCard', favouriteCardSchema);
