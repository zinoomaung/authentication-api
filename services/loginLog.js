const LoginLog = require('../model/loginLog');

const createLoginLog = (loginInfo) => {
    const newLoginLog = new LoginLog(loginInfo);
    return newLoginLog.save();
};

module.exports = {
    createLoginLog,
};
