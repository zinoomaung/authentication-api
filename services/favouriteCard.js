const FavouriteCard = require('../model/favouriteCard');

const createOrDelete = async (favourite) => {
    const createdFavourite = await FavouriteCard.findOne({ cardId: favourite.cardId, user: favourite.user });

    if (createdFavourite && favourite.isFavourite == false) {
        return FavouriteCard.findByIdAndDelete(createdFavourite._id);
    } else if (!createdFavourite && favourite.isFavourite) {
        const newFavourite = new FavouriteCard(favourite);
        return newFavourite.save();
    } else {
        return { success: true };
    }
};

const getFavouritesByUser = (userId) => {
    return FavouriteCard.find({ user: userId });
};

module.exports = {
    createOrDelete,
    getFavouritesByUser,
};
