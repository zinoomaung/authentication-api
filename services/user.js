const User = require('../model/user');

const deleteUserById = (userId) => {
    return User.findByIdAndDelete(userId);
};

const getAllUsers = () => {
    return User.find();
};

const getOneById = (userId) => {
    return User.findOne({ _id: userId }).select('+password');
};

const getUserByEmail = (email) => {
    return User.findOne({ email }).select('+password');
};

const getUserById = (userId) => {
    return User.findById(userId);
};

const getUserByResetToken = (user) => {
    return User.findOne(user);
};

const registerUser = (userInfo) => {
    const newUser = new User(userInfo);
    return newUser.save();
};

const updateUserById = (userId, user) => {
    return User.findByIdAndUpdate(userId, user, { new: true });
};

module.exports = {
    deleteUserById,
    getAllUsers,
    getOneById,
    getUserByEmail,
    getUserById,
    getUserByResetToken,
    registerUser,
    updateUserById,
};
