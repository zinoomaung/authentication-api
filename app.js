var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var morganLogger = require('morgan');

// application
var app = express();

// port
const port = 9000;

// routers
var authRouter = require('./routes/auth');
var userRouter = require('./routes/user');
var favouriteCardRouter = require('./routes/favouriteCard');

// modules
const { db } = require('./config/config');

// authentication
const { verifyUserToken } = require('./middleware/authentication');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// packages
app.use(morganLogger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// custom packages
const mongoose = require('mongoose');

// connect to MongoDB
mongoose.connect(db.uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}).then(() => {
    console.log('MongoDB Connected!');
}).catch((error) => {
    console.log('MongoDB Connect Fail : ', error);
});

// route groups
app.use('/', authRouter);
app.use('/user', verifyUserToken, userRouter);
app.use('/favourite-card', verifyUserToken, favouriteCardRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

app.listen(port);

module.exports = app;
