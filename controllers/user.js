const bcrypt = require('bcrypt');
const message = require('../constants/message');
const userService = require('../services/user');

const getAllUsers = async (request, response, next) => {
    try {
        const users = await userService.getAllUsers();

        if (!users) {
            response.status(404).json({
                errorMessage: message.MESSAGE_4,
                success: false,
            });
        } else {
            response.status(200).json({
                data: users,
                success: true,
            });
        }
    } catch (error) {
        response.status(500).json({
            errorMessage: message.MESSAGE_11,
            success: false,
        });
    }
};

const getUserById = async (request, response, next) => {
    try {
        const userId = request.params.userId;

        if (!userId) {
            response.status(400).json({
                errorMessage: message.MESSAGE_1,
                success: false,
            });
        }

        const user = await userService.getUserById(userId);

        if (!user) {
            response.status(404).json({
                errorMessage: message.MESSAGE_4,
                success: false,
            });
        } else {
            response.status(200).json({
                data: user,
                success: true,
            });
        }
    } catch (error) {
        response.status(500).json({
            errorMessage: message.MESSAGE_11,
            success: false,
        });
    }
};

const updateUserById = async (request, response, next) => {
    try {
        const userId = request.params.userId;

        if (!userId) {
            response.status(400).json({
                errorMessage: message.MESSAGE_1,
                success: false,
            });
        }

        const user = request.body;
        const updated = await userService.updateUserById(userId, user);

        if (!updated) {
            response.status(404).json({
                errorMessage: message.MESSAGE_4,
                success: false,
            });
        } else {
            response.status(200).json({
                data: updated,
                success: true,
            });
        }
    } catch (error) {
        response.status(500).json({
            errorMessage: message.MESSAGE_11,
            success: false,
        });
    }
};

const deleteUserById = async (request, response, next) => {
    try {
        const userId = request.params.userId;

        if (!userId) {
            response.status(400).json({
                errorMessage: message.MESSAGE_1,
                success: false,
            });
        }

        const deleted = await userService.deleteUserById(userId);

        if (!deleted) {
            response.status(404).json({
                errorMessage: message.MESSAGE_4,
                success: false,
            });
        } else {
            response.status(204).json({
                success: true,
            });
        }
    } catch (error) {
        response.status(500).json({
            errorMessage: message.MESSAGE_11,
            success: false,
        });
    }
};

const changePassword = async (request, response, next) => {
    try {
        const { currentPassword, newPassword, confirmPassword } = request.body;
        const userId = request.user.id;

        if (!currentPassword || !newPassword || !confirmPassword) {
            return response.status(400).json({
                errorMessage: message.MESSAGE_1,
                success: false,
            });
        } else if (newPassword !== confirmPassword) {
            return response.status(400).json({
                errorMessage: message.MESSAGE_2,
                success: false,
            });
        } else if (newPassword == currentPassword) {
            return response.status(400).json({
                errorMessage: message.MESSAGE_5,
                success: false,
            });
        }

        const validUser = await userService.getOneById(userId);

        if (!validUser) {
            response.status(404).json({
                errorMessage: message.MESSAGE_4,
                success: false,
            });
        } else {
            const validPassword = await bcrypt.compare(currentPassword, validUser.password);
            if (validPassword) {
                const salt = await bcrypt.genSalt(10);
                validUser.password = await bcrypt.hash(newPassword, salt);
                validUser.save();

                response.status(200).json({ success: true });
            } else {
                response.status(400).json({
                    errorMessage: message.MESSAGE_6,
                    success: false,
                });
            }
        }
    } catch (error) {
        response.status(500).json({
            errorMessage: message.MESSAGE_11,
            success: false,
        });
    }
};

module.exports = {
    changePassword,
    deleteUserById,
    getAllUsers,
    getUserById,
    updateUserById,
};
