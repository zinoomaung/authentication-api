const message = require('../constants/message');
const favouriteCardService = require('../services/favouriteCard');

const createOrDelete = async (request, response, next) => {
    try {
        const { cardId, userId, isFavourite } = request.body;

        if (!cardId || !userId) {
            return response.status(400).json({
                errorMessage: message.MESSAGE_1,
                success: false,
            });
        }

        const favourite = {
            cardId,
            user: userId,
            isFavourite: isFavourite ? isFavourite : false,
        };

        const createdFavourite = await favouriteCardService.createOrDelete(favourite);

        if (!createdFavourite) {
            return response.status(400).json({
                errorMessage: message.MESSAGE_1,
                success: false,
            });
        }

        response.status(200).json({
            message: isFavourite ? message.MESSAGE_13 : message.MESSAGE_14,
            success: true,
        });
    } catch (error) {
        response.status(500).json({
            errorMessage: message.MESSAGE_11,
            success: false,
        });
    }
};

const getFavouritesByUser = async (request, response, next) => {
    try {
        const userId = request.params.userId;

        if (!userId) {
            response.status(400).json({
                errorMessage: message.MESSAGE_1,
                success: false,
            });
        }

        const favourites = await favouriteCardService.getFavouritesByUser(userId);

        response.status(200).json({
            data: favourites,
            success: true,
        });
    } catch (error) {
        response.status(500).json({
            errorMessage: message.MESSAGE_11,
            success: false,
        });
    }
};

module.exports = {
    createOrDelete,
    getFavouritesByUser,
};
