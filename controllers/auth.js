const { token } = require('../config/config');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const loginLogService = require('../services/loginLog');
const message = require('../constants/message');
const nodemailer = require('nodemailer');
const userService = require('../services/user');

const register = async (request, response, next) => {
    try {
        const { name, email, password, confirmPassword } = request.body;

        if (!name || !email || !password || !confirmPassword) {
            return response.status(400).json({
                errorMessage: message.MESSAGE_1,
                success: false,
            });
        }

        if (password !== confirmPassword) {
            return response.status(400).json({
                errorMessage: message.MESSAGE_2,
                success: false,
            });
        }

        const salt = await bcrypt.genSalt(10);
        const hashPassword = await bcrypt.hash(password, salt);

        const user = {
            email,
            name,
            password: hashPassword,
        };

        const registeredUser = await userService.registerUser(user);

        if (!registeredUser) {
            return response.status(400).json({
                errorMessage: message.MESSAGE_1,
                success: false,
            });
        }

        const payload = { id: registeredUser._id };
        const JWToken = jwt.sign(payload, token.secret, token.options);

        response.status(200).json({
            JWToken,
            message: message.MESSAGE_12,
            success: true,
        });
    } catch (error) {
        if (error.code === 11000) {
            return response.status(400).json({
                errorMessage: message.MESSAGE_10,
                success: false,
            });
        }

        response.status(500).json({
            errorMessage: message.MESSAGE_11,
            success: false,
        });
    }
};

const logIn = async (request, response, next) => {
    try {
        const { email, password } = request.body;

        if (!email || !password) {
            return response.status(400).json({
                errorMessage: message.MESSAGE_1,
                success: false,
            });
        }

        const validUser = await userService.getUserByEmail(email);

        const loginInfo = {
            email,
            ip: request.connection.remoteAddress,
            userAgent: request.get('user-agent'),
            success: false,
            createdAt: getNow(),
        };

        if (validUser) {
            const validPassword = await bcrypt.compare(password, validUser.password);

            if (validPassword) {
                const payload = { id: validUser._id };
                const JWToken = jwt.sign(payload, token.secret, token.options);
                await loginLogService.createLoginLog({ ...loginInfo, success: true });

                return response.status(200)
                    .header('auth-token', JWToken)
                    .send({
                        email,
                        id: validUser._id,
                        name: validUser.name,
                        success: true,
                        token: JWToken,
                    });
            } else {
                await loginLogService.createLoginLog(loginInfo);

                return response.status(401).json({
                    errorMessage: message.MESSAGE_3,
                    success: false,
                });
            }
        } else {
            return response.status(401).json({
                errorMessage: message.MESSAGE_3,
                success: false,
            });
        }
    } catch (error) {
        response.status(500).json({
            errorMessage: message.MESSAGE_11,
            success: false,
        });
    }
};

const forgotPassword = async (request, response, next) => {
    try {
        const { email } = request.body;

        if (!email) {
            return response.status(400).json({
                errorMessage: message.MESSAGE_1,
                success: false,
            });
        }

        const validUser = await userService.getUserByEmail(email);

        if (validUser) {
            const resetPasswordToken = await bcrypt.genSalt(10);
            validUser.resetPasswordToken = resetPasswordToken;
            validUser.resetPasswordExpires = Date.now() + 3600000; // 1 hour
            await validUser.save();

            // send mail here

            return response.status(200).json({
                message: message.MESSAGE_7,
                resetPasswordToken: resetPasswordToken,
                success: true,
            });
        } else {
            return response.status(401).json({
                errorMessage: message.MESSAGE_4,
                success: false,
            });
        }
    } catch (error) {
        response.status(500).json({
            errorMessage: message.MESSAGE_11,
            success: false,
        });
    }
};

const resetPassword = async (request, response, next) => {
    try {
        const { email, password, confirmPassword, resetPasswordToken } = request.body;

        if (!email || !password || !confirmPassword || !resetPasswordToken) {
            return response.status(400).json({
                errorMessage: message.MESSAGE_1,
                success: false,
            });
        }

        if (password !== confirmPassword) {
            return response.status(400).json({
                errorMessage: message.MESSAGE_2,
                success: false,
            });
        }

        const user = {
            email,
            resetPasswordToken,
            resetPasswordExpires: { $gt: Date.now() },
        };

        const validUser = await userService.getUserByResetToken(user);

        if (validUser) {
            const salt = await bcrypt.genSalt(10);
            const hashPassword = await bcrypt.hash(password, salt);

            validUser.password = hashPassword;
            validUser.resetPasswordToken = null;
            validUser.resetPasswordExpires = null;
            await validUser.save();

            return response.status(200).json({
                message: message.MESSAGE_9,
                success: true,
            });
        } else {
            return response.status(401).json({
                errorMessage: message.MESSAGE_8,
                success: false,
            });
        }
    } catch (error) {
        response.status(500).json({
            errorMessage: message.MESSAGE_11,
            success: false,
        });
    }
};

const getNow = () => {
    const now = new Date();
    const date = new Date(now.getTime() - now.getTimezoneOffset() * 60000).toISOString();

    return date.replace(/T/, ' ').replace(/\..+/, '');
};

module.exports = {
    forgotPassword,
    logIn,
    register,
    resetPassword,
};
