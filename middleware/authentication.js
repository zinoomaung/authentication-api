const { token } = require('../config/config');
const jwt = require('jsonwebtoken');
const message = require('../constants/message');

const verifyUserToken = (request, response, next) => {
    try {
        let userToken = request.headers._token;

        if (userToken) {
            userToken = userToken.split(' ')[1];

            if (userToken === 'null' || userToken === undefined || !userToken) {
                return response.status(401).json({
                    errorMessage: message.MESSAGE_0,
                    success: false,
                });
            }

            const verifiedUser = jwt.verify(userToken, token.secret);

            if (verifiedUser) {
                if (verifiedUser.exp && verifiedUser.exp > Date.now() / 1000) {
                    request.user = verifiedUser;
                    next();
                } else {
                    return response.status(401).json({
                        errorMessage: message.MESSAGE_8,
                        success: false,
                    });
                }
            } else {
                return response.status(401).json({
                    errorMessage: message.MESSAGE_0,
                    success: false,
                });
            }
        } else {
            return response.status(401).json({
                errorMessage: message.MESSAGE_0,
                success: false,
            });
        }
    } catch (error) {
        response.status(500).json({
            errorMessage: message.MESSAGE_11,
            success: false,
        });
    }
};

module.exports = {
    verifyUserToken,
};
