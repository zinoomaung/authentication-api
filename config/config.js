const dotenv = require('dotenv');
dotenv.config();

const currentEnv = process.env.NODE_ENV || 'local';

console.log('Config Exported!');

exports.appName = 'AuthenticationAPI';

exports.db = { uri: process.env.MONGODB_URL };

exports.token = {
    secret: process.env.TOKEN_SECRET,
    options: { expiresIn: '1d' },
};

exports.env = {
    development: false,
    production: false,
    staging: false,
    testing: false,
};

exports.env[currentEnv] = true;
