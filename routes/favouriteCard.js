const express = require('express');

const router = express.Router();

const favouriteCardController = require('../controllers/favouriteCard');

router.get('/:userId', favouriteCardController.getFavouritesByUser);

router.post('/', favouriteCardController.createOrDelete);

module.exports = router;
