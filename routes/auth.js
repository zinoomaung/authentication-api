const express = require('express');
const router = express.Router();
const authController = require('../controllers/auth');

router.post('/register', authController.register);

router.post('/login', authController.logIn);

router.post('/forgot', authController.forgotPassword);

router.post('/reset', authController.resetPassword);

module.exports = router;
