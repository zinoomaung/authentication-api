const express = require('express');

const router = express.Router();

const userController = require('../controllers/user');

router.get('/', userController.getAllUsers);

router.get('/:userId', userController.getUserById);

router.patch('/:userId', userController.updateUserById);

router.delete('/:userId', userController.deleteUserById);

router.post('/change-password', userController.changePassword);

module.exports = router;
